import axios from '../axios-api';
export const FETCH_MESSAGES_SUCCESS = "FETCH_MESSAGES_SUCCESS";
export const CREATE_MESSAGES_SUCCESS = "CREATE_MESSAGES_SUCCESS";

export const PASS_CHANGE = 'PASS_CHANGE';
export const ENCODE_CHANGE = 'ENCODE_CHANGE';

export const encodeChange = (event) => ({type: ENCODE_CHANGE, encode: event.target.value});
export const passChange = (event) => ({type: PASS_CHANGE, pass: event.target.value});

export const fetchMessageSuccess = message =>({type:FETCH_MESSAGES_SUCCESS, message});
export const createMessageSuccess = () =>({type: CREATE_MESSAGES_SUCCESS});


export const encodeMessage = (data) => {
    return dispatch => {
        return axios.post('/encode', data).then(response => {
            dispatch(fetchMessageSuccess(response.data.encoded));
        })
    }
};