import {FETCH_MESSAGES_SUCCESS, PASS_CHANGE, ENCODE_CHANGE} from  "./actions"

const initialState ={
    encode: '',
    decode:'',
    password: ''
};

const reducer = (state = initialState,action) =>{
    switch (action.type) {
        case FETCH_MESSAGES_SUCCESS:
            return {
                ...state,
                decode: action.message
            };
        case PASS_CHANGE:
            return {...state, password: action.pass};
        case ENCODE_CHANGE:
            return {...state, encode: action.encode};
        default: return state

    }
}
export default reducer;

