import React, {Component, Fragment} from 'react';
import {Button,Col,Form,FormGroup,Label, Input} from "reactstrap"
import {connect} from 'react-redux';
import {encodeMessage, encodeChange, passChange} from "../../store/actions";

class CodeDecodeForm extends Component {

    submitFormHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    encodeHandler = () => {
        const data = {
            message: this.props.encode,
            password: this.props.password
        };
        this.props.encodeMessage(data)
    };

    render() {
        return (
                <Fragment>
                    <FormGroup row>
                        <Label sm={2} for="word">Word to code:</Label>
                        <Col sm={10}>
                            <Input
                                type="text" required
                                name="word" id="word"
                                placeholder="Enter word to code"
                                value={this.props.encode}
                                onChange={this.encodeChange}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="password">Word to code:</Label>
                        <Col sm={10}>
                            <Input
                                type="text" required
                                name="password" id="password"
                                placeholder="Enter password"
                                value={this.props.password}
                                onChange={this.props.passChange}
                            />
                            <Button onClick={this.encodeHandler}>Code</Button>
                            <Button>Decode</Button>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="word">Word to decode:</Label>
                        <Col sm={10}>
                            <Input
                                type="text" required min="0"
                                name="word" id="word"
                                placeholder="Enter word to decode"
                                value={this.props.decode}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        encode: state.encode,
        password: state.password,
        decode: state.decode
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        encodeMessage: (data) => dispatch(encodeMessage(data)),
        encodeChange: (event) => dispatch(encodeChange(event)),
        passChange: (event) => dispatch(passChange(event))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CodeDecodeForm);