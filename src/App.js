import React, { Component,Fragment } from 'react';
import {Route, Switch} from "react-router-dom";
import {Container} from "reactstrap";
import CodeDecodeForm from './components/CodeDecodeForm/CodeDecodeForm';
import './App.css';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Container>
          <Switch>
            <Route path="/" exact component={CodeDecodeForm}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;
